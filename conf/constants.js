const path = require('path');

const BASE_DIR = path.normalize(`${__dirname}/..`);
const TMP_DIR = `${BASE_DIR}/.tmp`;
const TMP_AFP_DIR = `${TMP_DIR}/AFP_WORK`;

module.exports = {
    BASE_DIR,
    TMP_DIR,
    TMP_AFP_DIR,
};