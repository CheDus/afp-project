{
    "restartable": "rs",
    "ignore": [
        ".git",
        "node_modules/**/node_modules",
        ".temp"
    ],
    "verbose": true
}
