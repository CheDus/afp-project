const fs = require('fs');
const path = require('path');
const Generator = require('./lib/Keywords/Generator');
const FsTools = require('./lib/FsTools');
const Robot = require('./lib/Keywords/Robot');
const { BASE_DIR, TMP_SELENIUM_DIR } = require('./conf/constants');

const keywordsFile = `${BASE_DIR}/.keywords.json`;

async function ParseSeleniumLibrary() {
    await Generator.buildRawkeywordsList().then(() => {
        console.debug('//////////////////////// End conversion');
    }).catch((err) => console.error(err));
};

async function Transpilation(file) {
    if (!path.extname(file).match(/txt|robot/)) {
        return true;
    }
    const robot = new Robot(file);
    if (!robot.isRobotFile()) {
        return true;
    }
    if (!robot.isFormattedWithPipe()) {
        await Robot.convertToRobot(file, { format: 'txt' });
    }

    console.debug('))))))))))) START EXTRACT ', file)
    robot.extractAll();
    console.debug('))))))))))) END EXTRACT ', file)
    console.debug(robot.output)
}

async function main() {
    try {
        console.debug("\n\n\n\n\n############################################################################################");
        if (!fs.existsSync(keywordsFile)) {
            console.debug("############################################################## LOAD KEYWORDS FRON REPO");
            await Generator.cloneSource(ParseSeleniumLibrary);
            await FsTools.writeJsonFileSync(keywordsFile, Generator.getRawKeywords());
        } else {
            console.debug("############################################################## LOAD KEYWORDS FROM FILE");
            Generator.setRawKeywords(await FsTools.readJsonFileSync(keywordsFile));
        }
        // console.debug(Generator.getRawKeywords());

        const src = TMP_SELENIUM_DIR + '/bo/02__Form_Elements/02__Test_Duplicate_Form_Name.robot';
        console.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> start convert');

        // await Robot.testRobotTidy();
        // await Robot.convertToRobot(TMP_SELENIUM_DIR);
        // await Robot.convertToRobot(TMP_SELENIUM_DIR + '/data/keywords.txt', { format: 'txt' });

        // await Transpilation(src);

        await FsTools.scanDirRecursive(TMP_SELENIUM_DIR, Transpilation);

        console.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> end convert');

    } catch (err) {
        console.error(err);
    }

}

main();
